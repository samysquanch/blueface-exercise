import { Component, OnInit } from '@angular/core';
import { I18nService } from './i18n-service/i18n.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	public title: string = 'blueface-exercise';
	public currentLanguage: string = 'en';

	constructor(
		private i18nService: I18nService
	) {}

	public ngOnInit(): void {
		this.i18nService.loadTranslationFile(this.currentLanguage); // language id could be fetched from browser locale
	}

	public loadLang(langId: string): void {
		this.currentLanguage = langId;
		this.i18nService.loadTranslationFile(this.currentLanguage);
	}
}

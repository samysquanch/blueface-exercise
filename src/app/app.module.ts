import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { ProfileService } from './profile-service/profile.service';
import { I18nService } from './i18n-service/i18n.service';
import { I18nPipe } from './i18n-pipe/i18n.pipe';

@NgModule({
	declarations: [AppComponent, ProfileSettingsComponent, I18nPipe],
	imports: [BrowserModule, FormsModule],
	providers: [ProfileService, I18nService],
	bootstrap: [AppComponent],
})
export class AppModule {}

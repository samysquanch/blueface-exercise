import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../i18n-service/i18n.service';

@Pipe({
	name: 'i18',
	pure: false // not ideal, only for proof of concept
})
export class I18nPipe implements PipeTransform {

	public translations: object;

	constructor(private i18nService: I18nService) {}

	public transform(labelKey: any): string {
		return this.i18nService.getTranslatedValue(labelKey);
	}
}

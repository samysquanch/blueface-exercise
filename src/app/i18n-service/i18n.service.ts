import { Injectable } from '@angular/core';

@Injectable()
export class I18nService {

	public translations: object;

	constructor() {}

	public loadTranslationFile(lang): void {
		switch (lang) {
			case 'en':
				this.translations = {
					'label.profile.username': 'Username',
					'label.profile.email': 'Email',
					'label.profile.firstname': 'First name',
					'label.profile.lastname': 'Last name',
					'label.profile.saving': 'Saving profile...',
					'label.profile.loading': 'Loading profile...',
					'label.profile.error': 'Error!',
				};
				break;
			case 'fr':
				this.translations = {
					'label.profile.username': 'Le Username',
					'label.profile.email': 'Le Email',
					'label.profile.firstname': 'Le First name',
					'label.profile.lastname': 'Le Last name',
					'label.profile.saving': 'Le Saving profile...',
					'label.profile.loading': 'Le Loading profile...',
					'label.profile.error': 'Le Error!',
				};
				break;
		}
	}

	public getTranslatedValue(key: string): string {
		if (this.translations && this.translations.hasOwnProperty(key)) {
			return this.translations[key];
		} else {
			return 'translation key not found';
		}
	}
}

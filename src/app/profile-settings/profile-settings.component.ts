import { Component, OnInit } from '@angular/core';
import { IProfile, ProfileService } from '../profile-service/profile.service';

@Component({
	selector: 'app-profile-settings',
	templateUrl: './profile-settings.component.html',
	styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent implements OnInit {

	public title: string = 'Profile';
	public user: IProfile;
	public errorMessage: string;
	public isSaving: boolean;
	public previousFirstName: string;
	public previousLastName: string;

	constructor(private profile: ProfileService) {}

	public ngOnInit(): void {
		this.attemptUserRequest();
	}

	public attemptUserRequest(): void {
		this.profile.getProfileUser().then( (user: IProfile) => {
			console.log(user);
			this.previousFirstName = user.firstName;
			this.previousLastName = user.lastName;
			this.updateDisplay(user);
		}, error => {
			console.log(error);
			this.errorMessage = error.error + ', attempting to contact server again...';
			this.attemptUserRequest();
		});
	}

	public saveProfile(): void {
		this.errorMessage = null;
		this.isSaving = true;
		this.profile.setName(this.user.firstName, this.user.lastName)
			.then( (user: IProfile) => {
				this.previousFirstName = user.firstName;
				this.previousLastName = user.lastName;
				this.updateDisplay(user);
				this.generateEmail();
			}, error => {
				this.errorMessage = error.error;
			})
			.then(() => this.isSaving = false);
	}

	public updateFirstName(newFirstName: string): void {
		this.user.firstName = newFirstName.trim();
	}
	public updateLastName(newLastName: string): void {
		this.user.lastName = newLastName.trim();
	}

	public generateEmail(): void {
		console.log('generating email');
		const validatedEmail = `${this.user.firstName.replace(' ', '')}.${this.user.lastName.replace(' ', '')}@blueface.com`;
		this.profile.setUserEmail(validatedEmail).then((user: IProfile) => {
			this.updateDisplay(user);
		}, error => {
			this.errorMessage = error.error;
			this.revertUserName();
		});
	}

	private updateDisplay(user: IProfile): void {
		this.user = user;
		this.errorMessage = null;
	}

	private revertUserName(): void {
		console.log('reverting user name');
		this.isSaving = true;
		this.profile.setName(this.previousFirstName, this.previousLastName)
			.then( (user: IProfile) => {
				this.updateDisplay(user);
			}, error => {
				this.errorMessage = error.error;
			})
			.then(() => this.isSaving = false);
	}
}
